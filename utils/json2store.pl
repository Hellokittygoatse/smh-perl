#!/usr/bin/perl
#
# json2store.pl
# 
# Utility for Social Media Hider
#
# Takes a suitable JSON structure and stores in a erl native format that
# is quicker to load and read. The JSON may be read from a file or from
# STDIN so you can pipe it from parsesubnets.pl 
#
# Requires JSON. On Ubuntu do 
# sudo apt install libjson-perl
#

use Storable;
use JSON;

my ($ofile, $t);
if ($#ARGV <0) {
	print "Usage $0 file.json outfile.store\n somecommand | $0 outfile.store";
	exit;
} elsif ($#ARGV >0) {
	my $f= shift;
	$ofile = shift;
	open (FILE, "<$f") or die "Can't open $f to read";
	$t=decode_json(<FILE>);
	close(FILE);
} else {
	$ofile = shift;
	$t=decode_json(<STDIN>);
}
store ($t, $ofile)

