#!/usr/bin/perl
#
# scrapeasn.pl
# 
# Utility for Social Media Hider
#
# Gets all the subnets for one or more ASNs from the bigdatacloud.com page(s). 
# Output to stdout
# usage: scrapeasn.pl 13414 32934 >smhnets.txt 
#

if ($#ARGV <0) {
	print "Usage $0 ASN [ASN2 ...]\n";
	exit;
}

while (@ARGV) {
	my $asn=shift;
	$_=`curl https://www.bigdatacloud.com/asn-lookup/AS$asn/prefixes`;
	my $mp=1; #max page.
	for my $p (m!page=(\d+)!sg) {
		$mp=$p if ($p>$mp);
	}

	my $p=1; #current page (page 1)
	while ($p<=$mp) {
	for my $n (m!>(\d+\.\d+\.\d+\.\d+\/\d+)!sg) {
		print $n.$/;
	}
	$p++;
	last if ($p>$mp);
	$_=`curl https://www.bigdatacloud.com/asn-lookup/AS$asn/prefixes?page=$p`;
	}
}
