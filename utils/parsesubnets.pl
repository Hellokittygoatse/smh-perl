#!/usr/bin/perl
#
# parsesubnets.pl 
# 
# Utility for Social Media Hider
#
# Takes a text file with a list of subnets one per line (other stuff
# can be on the line too, this will be ignored) and creates a suitable
# JSON structure for quick look up to see if a particular subnet is to 
# be blocked. The JSON is output on STDOUT so you can pipe it into 
# json2store.pl
#
# Requires Net::CIDR::Lite and JSON. On Ubuntu do 
# sudo apt install libnet-cidr-lite-perl libjson-perl
#
use Net::CIDR::Lite;
use JSON;

my $c= Net::CIDR::Lite->new;

my $f= shift or die "Usage $0 listofsubnets.txt";
open (FILE, "<$f") or die "Can't open $f to read";
my $t=join('',<FILE>);
for (split (/\s*\n\s*/, $t )) {
	next unless (/(\d+\.\d+\.\d+\.\d+\/\d+)/);
	$c->add($1);
}
my %n;
for ($c->list_short_range() ) {
	/(\d+\.\d+)\.(\d+)\.0-255/; # note assumption that at least a /24
	if (! defined ($n{$1})) {
		 $n{$1}=[(0) x 256];
	}
	$n{$1}[int($2)]=1;
}
print encode_json(\%n);

