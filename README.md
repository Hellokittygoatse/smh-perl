# Social Media Hider

Social Media Hider is a way to publish links to external web content to censorious social media platforms like Twitter and Facebook that they won't censor because their censorbots and employees won't see the actual link but just a cover image.

## How it works
SMH looks like a classic URL shortener, and indeed it *is* a URL shortener, but it has an extra feature that decides whether to redirect to the target URL depending on what the IP address making the request is. If the source IP is in a subnet that the SMH operator doesn't trust then instead of returning the actual target URL it returns a decoy image or link

SMH consists of a redirect template that is readily customized, a very simple URL shortener page and script and a couple of utility scripts that take a list of IP subnets scraped from somewhere handy (e.g. https://bgp.he.net/ or https://www.bigdatacloud.com/asn-lookup ) and turns them into something that can be quickly scanned to see if the visitor is on the blocked list or not. 

Note that the scripts as written are IPv4 only and assume that the bad IP list is subnets that are at least /24s and at most /16s.

## Requirements
Perl and various perl libraries. Not all the libraries are needed for all the scripts and each script lists what it needs that is not installed by default. Specifically the actual redirect script only needs Storable.pm which is a default perl library, and the redirect creator script needs CGI.pm.

A web server that can execute perl scripts quickly. Tested using lighttpd on versions of ubuntu, but should be readily adaptable to other web servers and OSes. The lighttpd.conf.sample file contains the magic lines needed for redirection to work.

## Installation

- Download the files and run the address utils to get your list of bad subnets as a .store file
- Copy the .store file and the shortener creator script to your webserver in a suitable subdirectory (e.g. /var/www/html/r ) along with the decoy image(s)
- edit the $baseurl and possibe $basepath variables in the smh.pl and smhtplt.txt files
- make sure that the www-data user can create files and subdirectories in this subdirectory (chown and chmod)
- add the lines from lighttpd.conf.sample file to the right bits of your /etc/lighttpd that you can execute scripts in /var/www/html/r and below - remember to change the domain from example.com 

Share and Enjoy

