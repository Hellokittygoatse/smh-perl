#!/usr/bin/perl
#
# smh.pl
# 
# URL Shortener Generator for Social Media Hider
#
# CGI script that takes a supplied URL parameter (either direct as a get
# request with the smh.pl?url=http://somewhere.censored.com/page.html or
# as a post from the form created by the CGI script 
#
# Requires CGI, CGI::Carp, Cwd, File::Path annd MIME::Base64
# On Ubuntu all except CGI* appear to be standard modules included in 
# the base perl install. To install CGI and CGI::Carp do
#
# sudo apt install libcgi-pm-perl
#
# The script generates a 4 letter/digit base64 encoding of lower 24 bits
# of the unix epoch time that the script was run. It the creates an 
# executable script based on the template in $basepath/A/B/C/D.pl (where 
# ABCD is the 4 base64 charatcers fot the time. As this is the lower 24
# bits you will get duplicates after about 6 months (194 days) and this
# script makes no attempt to check if another one exists that may be
# overwritten. You will also get a conflict if two people submit a URL 
# at the same second. A newer version will probably add checking code to
# handle these cases.
#
# The two variables to change are $baseurl - which should be the host
# name and initial path of the server the script is installed on and
# $basepath which is the place where it runs 
#
use CGI ':standard';
use CGI::Carp qw/fatalsToBrowser/;
use Cwd;
use MIME::Base64;
use File::Path;

my $basepath='/var/www/html/r';
my $baseurl='http://example.com/_';

print header (-type=>'text/html',-charset=>'UTF-8').
    start_html('Social Media Hiding Tiny URL Creator').
    h3('Social Media Hiding FB-evading Tiny URL Creator');

#
# We have a URL parameter so create the shortener. #
if (param('url')) {
# If the method is GET
# then we need to simply remove the 'url=' from the query string. If you
# don't do that you get odd results when the url also has a query string
	my $url=(($ENV{'QUERY_STRING'}=~/url=/) ?substr($ENV{'QUERY_STRING'},index($ENV{'QUERY_STRING'},'url=')+4) :param('url'));
# get the time, round to 24 bits, encode and then replace + and / in the 
# standard base64 characterset with - and _
	$_=time & 0xFFFFFF; # 2^24 seconds = 194 days
	my $e = encode_base64(substr(pack ("L",$_),0,3)); # discard the upper 00 byte
	$e=~tr!+/!-_!; # make it file/url safe
# create the filename 
	$fn = $basepath.'/'.join('/',split('',substr($e,0,4))).'.pl';
# create the path if it doesn't exist
	mkpath (substr($fn,0,rindex($fn,'/')));
# read in the template, and put the URL in the placeholder 
	open (FILE, "<$basepath/smhtplt.txt") or die "can't open tplt";
	$_ = join ('',<FILE>);
	close (FILE);
	s/Location: %URL%/Location: $url/s;
# write out the file
	open (FILE, ">$fn");
	print FILE $_;
	close (FILE);
	chmod  0755, $fn;
# now report the shortened URL to the user
	$_=substr($fn,length($basepath)+1);
	s!(.)/(.)/(.)/(.)\.pl!$1$2$3$4!;
	print $url.br."is also known as".br.
	  a({-href=>$baseurl.$_},$baseurl.$_).br.' share &amp; enjoy...'.hr;
}

# The form to submit a URL for shortening and decoying. Plus some help text

print p,
    start_form,
    "URL to shorten: ", textfield('url','',80),
#   add options like which decoy to use etc. here if you
#   decide to improve the form
    submit('Create'),
    end_form.<<EOT.end_html;
<p>This page is a way to share links to external web content on censorious 
social media platforms like Twitter and Facebook that they won't censor 
because their censorbots and employees won't see the actual link but 
just the decoy image below.
</p>
<p><img src="smhdecoy.png"></p>
<h3>How it works</h3>
In most ways it looks like a classic URL shortener, and indeed it is a 
URL shortener, but it has an extra feature that decides whether to 
redirect to the target URL depending on what the IP address making the 
request is. If the source IP is in a subnet that belongs to facebook or
twitter then instead of returning the actual target URL it returns the 
decoy image</p>
<p>
This code comes from the following gitlab repo: 
<a href="https://gitlab.com/Hellokittygoatse/smh-perl">https://gitlab.com/Hellokittygoatse/smh-perl</a>.
feel free to download and install your own copy</p>
EOT

